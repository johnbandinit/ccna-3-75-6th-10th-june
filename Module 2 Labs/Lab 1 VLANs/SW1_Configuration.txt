######Switch 1 Configuration#######
!
en
!
conf t
!
hostname SW1
!
vlan 10 
 name SALES
 exit
!
vlan 20
 name ACCOUNTING
 exit
!
vlan 30
 name MGMT
 exit
!
vlan 40
 name VOICE
 exit
!
do wr