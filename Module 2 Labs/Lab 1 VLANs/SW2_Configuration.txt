#####Switch 2 Configuration#####
!
en
!
conf t
!
hostname SW2
!
vlan 10 
 name SALES
 exit
!
vlan 20
 name ACCOUNTING
 exit
!
vlan 30
 name MGMT
 exit
!
vlan 40
 name VOICE
 exit
!
int gi0/1
 description ///ACCOUNTING DEPT\\\
 switchport mode access
 switchport access vlan 20
 exit
!
int gi0/0
 description ///SALES DEPT\\\
 switchport mode access
 switchport access vlan 10
 switchport voice vlan 40
 exit
!
do wr